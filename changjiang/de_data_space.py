import numpy as np
import pandas as pd
from pandas import DataFrame,Series

'''
data: dataframe, columns should only be: station(index), year, month, day, pre(is your data, but named 'pre')
geosta: dataframe, columns should only be: station(index), lat, lon
功能：要求，data必须是年月日的格式，全部年月日都要有。
对基本的32766做处理为nan
找到数据中所有的nan，存为“nan_all_data.csv”
对数据中nan值超过百分之十的站点直接删除，并导出一个excel，以供确定删除了多少站点。
用站点均值做填补nan，并对其进行掩码, 存为“no_nan_data.csv”以及“no_nan_geosta.csv”
'''
def masknan(data, geosta, fillna=True):
    sy = int(data['year'].unique().min())
    ey = int(data['year'].unique().max())
    data = findnan(data, np.array(geosta.index.unique()), sy, ey)
    # data.to_csv('nan_all_data.csv')
    # data, geosta = delmanynan(data, geosta, sy, ey, fillna)
    return data, geosta


"data: dataframe, 要求colums里有‘year’,'pre'，index为站点号，"
def fchoose(data, sy, ey, sta, resetunit=1):
    if isinstance(sta, DataFrame) == True:
        sta = sta.index
    rdata = data[(data['year'] >= sy) & (data['year'] <= ey)]
    rdata = rdata.loc[sta]
    rdata['pre'] = rdata['pre']/resetunit
    return rdata


"rowdata:要求colums里有‘year’,'month', 'pre'，index为站点号"
def c_month(rowdata, month, dataname='c_month'):
    for number, i in enumerate(month):
        if i == sorted(i):
            n = len(i)
            if n == 2:
                data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1])]
            elif n == 3:
                data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2])]
            elif n == 4:
                data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2]) | (rowdata['month'] == i[3])]
            elif n == 5:
                data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2]) | (rowdata['month'] == i[3]) | (rowdata['month'] == i[4])]
            else:
                data = None
                print('month is longer than 5, please check or change your program')
            data.to_csv('%s.csv' % (dataname + str(i)))
        else:
            ssy = rowdata['year'].min()
            eey = rowdata['year'].max()
            data = rowdata.copy()
            n = len(i)
            id12 = i.index(max(i))
            if len(i[id12+1:]) == 1:
                data['year'][data['month'] == i[id12+1]] = np.array(data['year'][data['month'] == 1].apply(lambda x: x - 1))
            if len(i[id12 + 1:]) == 2:
                data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2])] = np.array(
                    data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2])].apply(
                        lambda x: x - 1))
            if len(i[id12 + 1:]) == 3:
                data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2]) | (data['month'] == i[id12+3])] = np.array(
                    data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2]) | (data['month'] == i[id12+3])].apply(
                        lambda x: x - 1))
            if len(i[id12 + 1:]) == 4:
                data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2]) | (data['month'] == i[id12+3]) | (data['month'] == i[id12+4])] = np.array(
                    data['year'][(data['month'] == i[id12+1]) | (data['month'] == i[id12+2]) | (data['month'] == i[id12+3]) | (data['month'] == i[id12+4])].apply(
                        lambda x: x - 1))

            if n == 2:
                data = data[(data['month'] == i[0]) | (data['month'] == i[1])]
            elif n == 3:
                data = data[(data['month'] == i[0]) | (data['month'] == i[1]) | (data['month'] == i[2])]
            elif n == 4:
                data = data[(data['month'] == i[0]) | (data['month'] == i[1]) | (data['month'] == i[2]) | (data['month'] == i[3])]
            elif n == 5:
                data = data[(data['month'] == i[0]) | (data['month'] == i[1]) | (data['month'] == i[2]) | (data['month'] == i[3]) | (data['month'] == i[4])]
            elif n == 6:
                data = data[(data['month'] == i[0]) | (data['month'] == i[1]) | (data['month'] == i[2]) | (data['month'] == i[3]) | (data['month'] == i[4]) | (data['month'] == i[5])]

            data = data[(data['year'] != ssy-1)]
            data = data[(data['year'] != eey)]
            data.to_csv('%s.csv' % (dataname + str(i)))
    return






















def delmanynan(data, sta, sy, ey, fillna):
    indexs = data[np.isnan(data['pre'])].index.unique()
    print(len(indexs),len(data.index.unique())/3)
    if len(indexs) < (len(data.index.unique())/3):
        data = data.drop(indexs)
        sta = sta.drop(indexs)
        print('the step is drop all nan')
    else:
        a = data['pre'].groupby(data.index).count()
        n = (ey + 1 - sy) * 365
        delindex = a[a < n - 50].index
        if (len(data.index.unique()) - len(delindex.unique())) > (len(data.index.unique())/10):
            data = data.drop(delindex)
            sta = sta.drop(delindex)
            print('cant drop all nan, drop nan more than 50')
        else:
            delindex = a[a < n - 500].index
            data = data.drop(delindex)
            sta = sta.drop(delindex)
            print('''too much 'nan', choose another way''')
    if fillna is True:
        # data['pre'] = np.ma.masked_invalid(data['pre'])
        print(data)
        data['pre'] = data['pre'].groupby([data.index, data['year'], data['month']]).transform(lambda x: x.fillna(x.mean()))
    return data, sta


def findnan(rowdata, station, sy, ey):
    sta, year, month, day = daystation(station, sy, ey)
    findnan = DataFrame(year, index=sta, columns=['year'])
    findnan['month'] = month
    findnan['day'] = day
    per = np.array([0] * len(sta))
    pernanindex = indexname(sta, year, month, day)
    pernan = Series(per.flatten(), index=pernanindex)
    perrowindex = indexname(np.array(rowdata.index), np.array(rowdata['year']), np.array(rowdata['month']), np.array(rowdata['day']))
    perrow = Series(np.array(rowdata['pre']), index=perrowindex)
    fine = pernan+perrow
    fine = fine.reindex(pernan.index)
    findnan['pre'] = np.array(fine)
    return findnan

def flattenlist(x):
    x = np.array(x)
    x = x.flatten()
    return x

def listflatten(x):
    x = np.array(x)
    x = x.flatten()
    return list(x)


def isleap(year):
    if ((year % 4 == 0) and (year % 100 != 0)) or year % 400 == 0:
        return 1
    else:
        return 0

def daystation(station, staryear, endyear):
    arnm = []
    anm = []
    rnm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    nm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    rmonth = []
    smonth = []
    for i in range(12):
        arnm.extend(list(range(1, rnm[i]+1)))
        rmonth.extend([i+1] * rnm[i])
        anm.extend(list(range(1, nm[i]+1)))
        smonth.extend([i+1] * nm[i])

    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    month = []
    day = []
    for i in range(staryear, endyear + 1):
        if isleap(i) == 1:
            day.extend(arnm)
            month.extend(rmonth)
            year.extend([i] * 366)
        else:
            day.extend(anm)
            month.extend(smonth)
            year.extend([i] * 365)
    for i in station:
        sta.extend([i] * len(year))
    day = day * nsta
    month = month * nsta
    year = year * nsta
    return np.array(sta).flatten(), np.array(year), np.array(month), np.array(day)


def allfindstations(station, staryear, endyear, month=None):
    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    if month == None:
        for i in station:
            sta.append([i] * nyear)
        for i in range(staryear, endyear+1):
            year.append([i])
        year = year * nsta
    else:
        nmonth = len(month)
        month = month * nyear
        for i in station:
            sta.append([i] * (nyear * nmonth))
        for i in range(staryear, endyear+1):
            year.append([i] * nmonth)
        year = year * nsta
        month = month * nsta
        month = flattenlist(month)

    sta = flattenlist(sta)
    year = flattenlist(year)

    return sta, year, month
def indexname(station, year, month=None, day=None):
    name = []
    month[np.where(month[:] == 1)] = 99
    for i in range(len(station)):
        name.append('%s' % (str(int(station[i])) + str(int(year[i])) + str(int(month[i])) + str(int(day[i]))))
    return name


