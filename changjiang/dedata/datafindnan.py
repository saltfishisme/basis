import numpy as np
import pandas as pd
from pandas import DataFrame,Series


def masknan(data, geosta, sy, ey, resetunit):
    '''
    data: dataframe, columns should only be: station(index), year, month, day, pre(is your data, but named 'pre')
    geosta: dataframe, columns should only be: station(index), lat, lon
    功能：要求，data必须是年月日的格式，全部年月日都要有。
    对基本的32766做处理为nan
    找到数据中所有的nan，存为“nan_all_data.csv”
    对数据中nan值超过百分之十的站点直接删除，并导出一个excel，以供确定删除了多少站点。
    用站点均值做填补nan，并对其进行掩码, 存为“no_nan_data.csv”以及“no_nan_geosta.csv”
    '''
    data['pre'] = stands(np.array(data['pre']), resetunit)
    data = findnan(data, np.array(geosta.index.unique()), sy, ey)
    data.to_csv('nan_all_data.csv')
    data, geosta = delmanynan(data, geosta, sy, ey)
    data.to_csv('no_nan_data.csv')
    geosta.to_csv('no_nan_geosta.csv')
    return data, geosta


def stands(data, resetunit):
    data = data/resetunit
    data[np.where(data == (32766/resetunit))] = np.nan
    return data


def delmanynan(data, sta, sy, ey):
    a = data['pre'].groupby(data.index).count()
    n = (ey + 1 - sy) * 365
    delindex = a[a < n - 20].index
    data = data.drop(delindex)
    sta = sta.drop(delindex)
    data['pre'] = np.ma.masked_invalid(data['pre'])
    fill_mean = lambda g: g.fillna(g.mean())
    data['pre'] = data['pre'].groupby(data.index).apply(fill_mean)
    return data, sta


def findnan(rowdata, station, sy, ey):
    sta, year, month, day = daystation(station, sy, ey)
    findnan = DataFrame(year, index=sta, columns=['year'])
    findnan['month'] = month
    findnan['day'] = day
    per = np.array([0] * len(sta))
    pernanindex = indexname(sta, year, month, day)
    pernan = Series(per.flatten(), index=pernanindex)
    perrowindex = indexname(np.array(rowdata.index), np.array(rowdata['year']), np.array(rowdata['month']), np.array(rowdata['day']))
    perrow = Series(np.array(rowdata['pre']), index=perrowindex)
    fine = pernan+perrow
    fine = fine.reindex(pernan.index)
    findnan['pre'] = np.array(fine)
    return findnan

def flattenlist(x):
    x = np.array(x)
    x = x.flatten()
    return x

def listflatten(x):
    x = np.array(x)
    x = x.flatten()
    return list(x)


def isleap(year):
    if ((year % 4 == 0) and (year % 100 != 0)) or year % 400 == 0:
        return 1
    else:
        return 0

def daystation(station, staryear, endyear):
    arnm = []
    anm = []
    rnm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    nm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    rmonth = []
    smonth = []
    for i in range(12):
        arnm.extend(list(range(1, rnm[i]+1)))
        rmonth.extend([i+1] * rnm[i])
        anm.extend(list(range(1, nm[i]+1)))
        smonth.extend([i+1] * nm[i])

    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    month = []
    day = []
    for i in range(staryear, endyear + 1):
        if isleap(i) == 1:
            day.extend(arnm)
            month.extend(rmonth)
            year.extend([i] * 366)
        else:
            day.extend(anm)
            month.extend(smonth)
            year.extend([i] * 365)
    for i in station:
        sta.extend([i] * len(year))
    day = day * nsta
    month = month * nsta
    year = year * nsta
    return np.array(sta).flatten(), np.array(year), np.array(month), np.array(day)


def allfindstations(station, staryear, endyear, month=None):
    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    if month == None:
        for i in station:
            sta.append([i] * nyear)
        for i in range(staryear, endyear+1):
            year.append([i])
        year = year * nsta
    else:
        nmonth = len(month)
        month = month * nyear
        for i in station:
            sta.append([i] * (nyear * nmonth))
        for i in range(staryear, endyear+1):
            year.append([i] * nmonth)
        year = year * nsta
        month = month * nsta
        month = flattenlist(month)

    sta = flattenlist(sta)
    year = flattenlist(year)

    return sta, year, month
def indexname(station, year, month=None, day=None):
    name = []
    month[np.where(month[:] == 1)] = 99
    for i in range(len(station)):
        name.append('%s' % (str(int(station[i])) + str(int(year[i])) + str(int(month[i])) + str(int(day[i]))))
    return name


if __name__ == "__main__":
    # rowdata = pd.read_csv('alldata.csv', index_col=0)
    # station = np.loadtxt('station.txt')
    # a = findnan(rowdata, station, 1, 1951, 2008)
    # a = a[(a['month']==8) | (a['month']==7)]
    # a['pre'].groupby([a.index, a['year'], a['month']]).sum().to_csv('nonandata.csv')
    data = pd.read_csv('nonandata.csv', index_col=0)
    data['pre'] = np.ma.masked_invalid(data['pre'])
    # def demen(arr):
    #     return arr - arr.mean()
    data['pre'] = data['pre']-data['pre'].mean()
    data['pre'].groupby([data.index, data['year']]).mean().to_csv('pre.csv')
