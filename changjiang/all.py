import numpy as np
import pandas as pd
from pandas import DataFrame
import sys
sys.path.append(r'D:\basis\changjiang\eof')
sys.path.append(r'D:\basis\changjiang\cor')
from eof import beof
from area_cor import choose,finddata
from scipy.interpolate import griddata
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

geosta = pd.read_csv(r'D:\a_postgraduate\changjiang\data\station_with_ll.csv', index_col=[0])
shps = np.loadtxt(r'D:\a_postgraduate\changjiang\data\shp.txt', usecols=[1, 2])

# month = [[7, 8, 9], [11,12,1, 2, 3,4]]
month = [[4,5,6], [11,12,1, 2, 3,4]]
name = ['汛期', '枯期']

month1 = pd.read_csv('c_month[7, 8, 9].csv')
month1 = month1[(month1['year'] != 2011)]
month2 = pd.read_csv('c_month[11, 12, 1, 2, 3, 4].csv')
pc = []
pc.append(np.array(month1['pre'].groupby(month1['year']).mean()))
pc.append(np.array(month2['pre'].groupby(month2['year']).mean()))
s=[0,1]
# finddata(1961, 2010, month[0], chgt=1, csst=1, cwnd=1, cice=0, moving_av=None)


# def fast_moving_average(x, N):
#     return np.convolve(x, np.ones((N,)) / N)[(N - 1):]
#
# # print(fast_moving_average(pc[0], 3).shape)
# for i in range(2):
#     aa, levels = schoose(fast_moving_average(pc[i], 10), s[i], name=name[i], chgt=0, csst=0, cwnd=0, cice=1, col='color'
#            , ranges=[[-89.5,89.5,0.5,359.5], [-60, -30, 0, 30, 60], [60,120,180,240,300]])

##############绘制除海冰外其他图像
# fig = plt.figure(figsize=[8,6])

# SST
# fig = plt.figure()
# for i in range(2):
#     aa, levels = schoose(pc[i], s[i], name=name[i], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#            , ranges=[[-89.5,89.5,0.5,359.5], [-60, -30, 0, 30, 60], [60,120,180,240,300]])
#     plt.tight_layout()
#     # plt.show()
#     plt.savefig('sst %s.png' %name[i])


# # fig = plt.figure()
# # fig = plt.figure(),
# fig, ax = plt.subplots(3,3, figsize=[9,7],subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
# plt.subplots_adjust(left=0.06, bottom=0.02, right=0.99, top=0.95, wspace=0.2, hspace=0.15)
# choose([ax[0][0], ax[1][0]], 1961, 2010, [4,5,6], [pc[0]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-1 '+name[0])
#
# choose([ax[0][1], ax[1][1]], 1961, 2010, [3,4,5], [pc[0]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-2 '+name[0])
#
# choose([ax[0][2], ax[1][2]], 1960, 2009, [2,3,4], [pc[0]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-3 '+name[0])
#
#
#
# choose([ax[2][0]], 1961, 2010, [4,5,6], [pc[0]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-1 '+name[0])
#
# choose([ax[2][1]], 1961, 2010, [3,4,5], [pc[0]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-1 '+name[0])
#
# choose([ax[2][2]], 1961, 2010, [2,3,4], [pc[0]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-3 '+name[0])
#
#
# plt.savefig('xun.png', dpi=800)


# fig, ax = plt.subplots(3,3, figsize=[9,7], subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
# plt.subplots_adjust(left=0.06, bottom=0.02, right=0.99, top=0.95, wspace=0.2, hspace=0.15)
# choose([ax[0][0], ax[1][0]], 1960, 2009, [6,7,8,9,10], [pc[1]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-1 '+name[1])
#
# choose([ax[0][1], ax[1][1]], 1960, 2009, [5,6,7,8,9], [pc[1]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-2 '+name[1])
#
# choose([ax[0][2], ax[1][2]], 1960, 2009, [4,5,6,7,8], [pc[1]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#        , ranges=[[65,0, 60, 150], [60,30,0], [60,90, 120, 150]], name='Lag-3 '+name[1])
#
#
# choose([ax[2][0]], 1960, 2009, [6,7,8,9,10], [pc[1]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-1 '+name[1])
#
# choose([ax[2][1]], 1960, 2009, [5,6,7,8,9], [pc[1]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-2 '+name[1])
#
# choose([ax[2][2]], 1960, 2009, [4,5,6,7,8], [pc[1]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#        , ranges=[[89.5, -89.5, 0.5, 359.5], [-60, -30, 0, 30, 60], [60, 120, 180, 240, 300]], name='Lag-3 '+name[1])
# plt.savefig('ku.png', dpi=800)


# for i in range(2):
#     fig = plt.figure()
#     plt.subplots_adjust(left=0.07, bottom=0.04, right=0.99, top=0.93, wspace=2, hspace=2)
#
#     if i == 0:
#         choose([fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))],
#                1961, 2010, [7,8,9], [pc[i]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#                , ranges=[[65, 0, 60, 150], [60, 30, 0], [65, 90, 120, 145]],
#                name=name[i])
#     if i == 1:
#         choose([fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))],
#                1960, 2009, [11,12,1,2,3,4], [pc[i]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
#                , ranges=[[65, 0, 60, 150], [60, 30, 0], [65, 90, 120, 145]],
#                name=name[i])
#
#     plt.tight_layout()
#     plt.savefig('hgt200 %i.png' %i)

# for i in range(2):
#     fig = plt.figure()
#     plt.subplots_adjust(left=0.07, bottom=0.04, right=0.99, top=0.93, wspace=2, hspace=2)
#
#     if i == 0:
#         choose([fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))],
#                1961, 2010, [7,8,9], [pc[i]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#                , ranges=[[65, -30, 40, 300], [60, 30, 0, -30], [60, 120, 180, 240, 300]],
#                name=name[i])
#     if i == 1:
#         choose([fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))],
#                1960, 2009, [11,12,1,2,3,4], [pc[i]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
#                , ranges=[[65, -30, 40, 300], [60, 30, 0, -30], [60, 120, 180, 240, 300]],
#                name=name[i])
#
#     plt.tight_layout()
#     plt.savefig('sst %i.png' %i)







allmonth = [[[4,5,6],[3,4,5],[2,3,4],
         [1,2,3],[12,1,2],[11,12,1],
         [10,11,12],[9,10,11],[8,9,10]],

[[5,6,7,8,9,10],[4,5,6,7,8,9],[3,4,5,6,7,8],[2,3,4,5,6,7],[1,2,3,4,5,6],[12,1,2,3,4,5],
         [11,12,1,2,3,4],[10,11,12,1,2,3],[9,10,11,12,1,2]]

]
lags = [[1,2,3],
        [4,5,6],
        [7,8,9]]

defi = 0
yearnumber = [5,6]

def main1(defi, sname):
    fig, ax = plt.subplots(nrows=3, ncols=3, sharey='row', sharex='col', figsize=[9, 7],
                           subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
    plt.subplots_adjust(left=0.06, bottom=0.02, right=0.99, top=0.99, wspace=0.12, hspace=0.0)

    month = allmonth[defi]
    change_year_number = yearnumber[defi]
    for i in range(3):
           for j in range(3):

                  count = i*3+j
                  if i*3+j+1 >=change_year_number:
                         choose([ax[i][j]], 1960, 2009, month[count], [pc[defi]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
                                , ranges=[[65, 0, 60, 150], [60, 30, 0], [65, 90, 120, 145]],
                                name='Lag-%i ' % lags[i][j] + name[defi])
                  else:
                         choose([ax[i][j]], 1961, 2010, month[count], [pc[defi]], chgt=0, csst=0, cwnd=1, cice=0, col='bw'
                                , ranges=[[65,0, 60, 150], [60,30,0], [65, 90, 120, 145]], name='Lag-%i '%lags[i][j]+name[defi])
    # plt.show()
    plt.savefig('%s.png'%sname)
    plt.close()
# main1(1, 'wnd200 ku')
# # main1(1, 'wnd200 ku')
# main1(0, 'wnd200 滞后 汛期')


def main2(defi, sname):
    fig, ax = plt.subplots(nrows=3, ncols=3, sharey='row', sharex='col', figsize=[9,4],subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
    plt.subplots_adjust(left=0.06, bottom=0.02, right=0.99, top=0.99, wspace=0.05, hspace=0.0)

    month = allmonth[defi]
    change_year_number = yearnumber[defi]
    for i in range(3):
           for j in range(3):
                  print('Lag-%i ' % lags[i][j] + name[defi])
                  count = i*3+j
                  if i*3+j+1 >=change_year_number:
                         choose([ax[i][j]], 1960, 2009, month[count], [pc[defi]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
                                , ranges=[[65, -30, 40, 300], [60, 30, 0, -30], [60, 120, 180, 240, 300]],
                                name='Lag-%i ' % lags[i][j] + name[defi])
                  else:
                         choose([ax[i][j]], 1961, 2010, month[count], [pc[defi]], chgt=0, csst=1, cwnd=0, cice=0, col='bw'
                               , ranges=[[65, -30, 40, 300], [60, 30, 0, -30], [60, 120, 180, 240, 300]], name='Lag-%i '%lags[i][j]+name[defi])
                  # plt.show()
    plt.show()
    plt.savefig('%s.png'%sname)
    plt.close()
# main2(0, 'sst xun')
main2(1, 'sst ku')

def main3(defi, sname):
    fig, ax = plt.subplots(nrows=3, ncols=3, sharey='row', sharex='col', figsize=[9, 7],
                           subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
    plt.subplots_adjust(left=0.06, bottom=0.02, right=0.99, top=0.99, wspace=0.12, hspace=0.0)

    month = allmonth[defi]
    change_year_number = yearnumber[defi]
    for i in range(3):
           for j in range(3):
                  print('Lag-%i ' % lags[i][j] + name[defi])
                  count = i*3+j
                  if i*3+j+1 >=change_year_number:
                         choose([ax[i][j]], 1960, 2009, month[count], [pc[defi]], chgt=1, csst=0, cwnd=0, cice=0, col='bw'
                                , ranges=[[65, 0, 60, 150], [60, 30, 0], [65,90, 120, 145]],
                                name='Lag-%i ' % lags[i][j] + name[defi])
                  else:
                         choose([ax[i][j]], 1961, 2010, month[count], [pc[defi]], chgt=1, csst=0, cwnd=0, cice=0, col='bw'
                                , ranges=[[65,0, 60, 150], [60,30,0], [65,90, 120, 145]], name='Lag-%i '%lags[i][j]+name[defi])

    plt.savefig('%s.png'%sname)
    plt.close()
# main3(1, 'hgt500 ku')
# main3(0, 'hgt200 xun')
# main3(1, 'hgt200 ku')


